" Plugin list
call plug#begin()

Plug 'kana/vim-textobj-indent'
Plug 'kana/vim-textobj-user'

Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'easymotion/vim-easymotion'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/vim-easy-align'
Plug 'lazywei/vim-matlab'
Plug 'lervag/vimtex'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'

if has('unix')
    Plug 'tpope/vim-eunuch'
endif

Plug 'rakr/vim-one'

call plug#end()

" Enable filetype detection, plugins and indent files
filetype plugin indent on

" Enable syntax highlighting
syntax enable

if has('gui_running')
    " Use one
    set background=dark
    colorscheme one

    " Set guifont
    set guifont=Source_Code_Pro:h11

    " Remove menubar, toolbar, left and right scrollbars
    set guioptions-=m
    set guioptions-=T
    set guioptions-=r
    set guioptions-=R
    set guioptions-=l
    set guioptions-=L
endif

" Change directories of temporary files
set backupdir=~/vimfiles/backup//
set directory=~/vimfiles/swap//
set undodir=~/vimfiles/undo//

" Show the line number of the current line
set number

" Change width of the gutter column
set numberwidth=6

" Highlight current line
set cursorline

" Allow backspacing over autoindent, line breaks and the start of insert
set backspace=indent,eol,start

" Show existing tabs as 4 spaces
set tabstop=4

" When indenting with '>', use 4 spaces
set shiftwidth=4

" When pressing tab, insert spaces instead
set expandtab

" When pressing backspace, delete up to 4 spaces
set softtabstop=4

" Copy indent from current line when starting a new line
set autoindent

" Always use shiftwidth for <Tab> and <BS> in front of a line
set smarttab

" Command line height
set cmdheight=2

" Required to show custom list symbols
set encoding=utf-8

" Change symbols for invisible characters
set listchars=eol:¬,tab:▸\ ,trail:·,precedes:←,extends:→

" Set leader to space
let mapleader=" "

" Set localleader to \
let maplocalleader="\\"

" Leader mappings
nnoremap <leader>= :call <SID>Preserve("normal gg=G")<CR>
nnoremap <leader>G :Goyo<CR>
nnoremap <leader>e :browse edit<CR>
nnoremap <leader>h :set hlsearch!<CR>
nnoremap <leader>l :set list!<CR>
nnoremap <leader>r :source $MYVIMRC<CR>
nnoremap <leader>v :edit $MYVIMRC<CR>
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>x :wq<CR>
nnoremap <leader>c :cd %:p:h<CR>

" Fugitive mappings
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>ge :Gedit<CR>
nnoremap <leader>gw :Gwrite<CR>
nnoremap <leader>gr :Gread<CR>

" Function to preserve "state" and execute command
" (Source: http://vimcasts.org/episodes/tidying-whitespace/)
function! <SID>Preserve(command)
    " Preparation: save last search, and cursor position.
    let l:win_view = winsaveview()
    let l:last_search = getreg('/')
    " Execute the command without adding to the
    " changelist/jumplist:
    execute 'keepjumps ' . a:command
    " Clean up: restore previous search history, and
    " cursor position
    call winrestview(l:win_view)
    call setreg('/', l:last_search)
endfunction

" Automatically strip whitespace when saving file
autocmd BufWritePre * :call<SID>Preserve("%s/\\s\\+$//e")

" When searching, show where the pattern typed so far matches
set incsearch

" Always display statusline
set laststatus=2

" Configure statusline
set statusline=                             " clear statusline for when vimrc is reloaded
set statusline+=%f                          " path to file in the buffer
set statusline+=%(\ %h%)                    " help flag: [Help] or empty
set statusline+=%(\ %m%)                    " modified flag: [+] or [-] or empty
set statusline+=%(\ %r%)                    " readonly flag: [RO] or empty
set statusline+=%(\ (%{fugitive#head()})%)  " current branch
set statusline+=%=                          " right align
set statusline+=\ Line:\ %l/%L              " current line and total number of lines
set statusline+=\ Column:\ %c                  " column number

" Enable enhanced command-line completion
set wildmenu

" Always show at least 5 lines above and below the cursor
set scrolloff=5

" Always show at least 5 columns left and right of the cursor
set sidescrolloff=5

" Disable line wrapping
set nowrap

" Show as much as possible of the last line in a window
set display+=lastline

" Delete comment character when joining commented lines
set formatoptions+=j

" Set shell to cmd.exe (this is needed to run vim as difftool in git for
" windows)
set shell=C:\WINDOWS\system32\cmd.exe

" When a file has been changed outside of Vim, but it has not been changed
" inside of Vim, automatically read it again
set autoread

" Enable timeout on key codes
set ttimeout

" Wait 100 ms for a key code or mapped key sequence to complete
set ttimeoutlen=100

" Allow hiding buffers with unsaved changes
set hidden

" Show current mode
set showmode

" Show currently typed command
set showcmd

" UltiSnips configuration
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsEditSplit="vertical"

" Disable default mappings
let g:EasyMotion_do_mapping = 0

" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nnoremap s <Plug>(easymotion-overwin-f2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" Use LaTeX by default
let g:tex_flavor="latex"

" Use SumatraPDF in Windows
if has('win32')
    let g:vimtex_view_general_viewer = 'SumatraPDF'
    let g:vimtex_view_general_options = '-reuse-instance -forward-search @tex @line @pdf'
    let g:vimtex_view_general_options_latexmk = '-reuse-instance'
endif

